'''
The function importData in this file is used to load the sessiontrack data.
It returns a list containing a dictionary for each interaction.
Each dictionary contains:
    'session': the session number
    'user': the user number
    'starttime': the start time of that interaction 
        (Each session starts with starttime=0)
    'query': the query that the user filled in
    'results': the clueweb ids of the top10 results
    'clicked': whether the user clicked on a result (boolean)
'''

import xml.etree.ElementTree as ET
from pprint import pprint

class Interaction:
    query = ''
    user = 0
    click = False
    starttime = 0
    results = []

    def __init__(self, element, session):
        self.extractFromElement(element)
        self.user = session[1]


def importData(url):
    xml = ET.parse(url)
    data = []
    recursiveTagSelection((0, 0), xml.getroot(), data)
    return data


def recursiveTagSelection(session, element, data):
    if element.tag == 'interaction':
        addInteraction(session, element, data)
        return

    if element.tag == 'session':
        session = (element.attrib['num'], element.attrib['userid'])

    for child in element:
        recursiveTagSelection(session, child, data)

def addInteraction(session, element, interactions):
    interaction = {}
    interaction['session'] = session[0]
    interaction['user'] = session[1]
    interaction['starttime'] = element.attrib['starttime']

    interaction['clicked'] = False
    interaction['results'] = []

    extractFromElement(interaction, element)
    interactions.append(interaction)

def extractFromElement(interaction, element):
    for child in element:
        if child.tag == 'query':
            interaction['query'] = child.text
        if child.tag == 'results':
            extractFromResult(interaction, child)
        if child.tag == 'clicked':
            interaction['clicked'] = True

def extractFromResult(interaction, results):
    for result in results:
        for child in result:
            if child.tag == 'clueweb12id':
                interaction['results'].append(child.text)

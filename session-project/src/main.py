'''
Information Retrieval Assignmnent
Niek Janssen, Evi Sijven en Wieske de Swart
'''
from data import importData
from similarity import baseline, stemstop, sentence_similarity
from measures import computeSimilarities, computeSimilaritiesLin
from evaluate import plotsim, accsim, ROC

# Import the sessiontrack data:
alldata = importData('../data/sessiontrack2014.xml')

# Create a list of the similarity functions:
simmeas = [baseline, stemstop, sentence_similarity]

# Compute the similarities between all query pairs:
# Choose a subset of the data:
# data = alldata[:180]  # first 50 sessions
# X, y = computeSimilarities(data, simmeas)

# Compute the similarities between query pairs that are close together
X, y = computeSimilaritiesLin(alldata, simmeas, nrcom=1)

# Plot the similarity measures:
for i in range(len(simmeas)):
    name = simmeas[i].__name__
    title = 'Histogram of ' + name
    plotsim(X[:, :, i], y, bins0=50, bins1=50, density=False, title=title)

# Compute accuracy:
accuracies = accsim(X, y, simmeas)

# Create ROC curve:
ROC(X, y, simmeas)

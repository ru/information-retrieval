'''
This file contains functions that are used to run over all query pairs
and computing the available similarity measures. 
'''
import numpy as np
from tqdm import tqdm

def computeSimilarities(data, simmeas):
    '''Computes similarity between all query pairs
    Note: for all (>3000) interactions this is very slow.
    Input:
        data = input data (with data['session'] = session nr)
        simmeas = list of similarity measures
    Output:
        X = (n,n,m) matrix with the similarity measure for each query
        y = (n,n) matrix with y[i,j]=1 if i and j in the same session'''
    N = len(data)  # nr of interactions
    m = len(simmeas)  # nr of similarity measures
    X = np.zeros((N, N, m))
    y = np.zeros((N, N))
    for i, a in enumerate(tqdm(data, desc='Compute similarity')):
        for j, b in enumerate(data):
            for k, m in enumerate(simmeas):
                # Compute similarity m between query a and b: 
                X[i,j,k] = (m(a, b))
            # Save whether a and b are in the same session: 
            y[i,j] = (a['session'] == b['session'])*1
    return X, y


def computeSimilaritiesLin(data, simmeas, nrcom=10):
    '''Computes similarity between a query and the nrcom queries before that query
    Input:
        data = input data (with data['session'] = session nr)
        simmeas = list of similarity measures
        nrcom = nr of queries before query that should be compared
    Output:
        X = (n,nrcom,m) matrix with the similarity measure for each query
        y = (n,nrcom) matrix with y[i,j]=1 if i and j in the same session'''
    N = len(data)  # nr of interactions
    m = len(simmeas)  # nr of similarity measures
    X = np.zeros((N, nrcom, m))
    y = np.zeros((N, nrcom))
    for i, a in enumerate(tqdm(data, desc='Compute similarity')):
        for j, b in enumerate(data[i-nrcom:i]):
            for k, m in enumerate(simmeas):
                # Compute similarity m between query a and b: 
                X[i,j,k] = (m(a, b))
            # Save whether a and b are in the same session: 
            y[i,j] = (a['session'] == b['session'])*1
    return X, y

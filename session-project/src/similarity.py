'''
This file contains the different similarity measures
The measures use the nltk package to stop/stem/analyze words
'''

# code from https://nlpforhackers.io/wordnet-sentence-similarity/
import nltk
nltk.download('punkt')
nltk.download('averaged_perceptron_tagger')
nltk.download('stopwords')
from nltk import word_tokenize, pos_tag
from nltk.corpus import wordnet as wn
from nltk.corpus import stopwords
from nltk.stem import PorterStemmer

def baseline(data1, data2):
    '''Baseline similarity measure
    This measure just compares the words in the two queries
    Input: two interactions (data1, data2)
    Output: nr of identical words/ nr of words in longer query'''
    # Get words from query:
    words1 = word_tokenize(data1['query'])
    words2 = word_tokenize(data2['query'])
    # Compare words in queries:
    same = 0
    for w1 in words1:
        test = [(w1==w2)*1 for w2 in words2]
        same += max(test, default=0)
    maxlength = max(len(words1), len(words2))
    return same/maxlength

def stemstop(data1, data2):
    '''Similarity measure that uses stemming and stopping
    This measure stems and stops the words in the query
    And returns the percentage of words occur in both queries
    Input: two interactions (data1, data2)
    Output: nr of identical words / nr of words in longer query'''
    # Get words from query:
    words1 = word_tokenize(data1['query'])
    words2 = word_tokenize(data2['query'])
    # Remove stop words and stem words:
    stop_words = set(stopwords.words('english'))
    ps = PorterStemmer()
    words1 = [ps.stem(w) for w in words1 if not w in stop_words]
    words2 = [ps.stem(w) for w in words2 if not w in stop_words]
    # Compare words in queries:
    same = 0
    for w1 in words1:
        test = [(w1==w2)*1 for w2 in words2]
        same += max(test, default=0)
    maxlength = max(len(words1), len(words2))
    return same/maxlength

def penn_to_wn(tag):
    """ Convert between a Penn Treebank tag to a simplified Wordnet tag """
    if tag.startswith('N'):
        return 'n'
    if tag.startswith('V'):
        return 'v'
    if tag.startswith('J'):
        return 'a'
    if tag.startswith('R'):
        return 'r'
    return None

def tagged_to_synset(word, tag):
    wn_tag = penn_to_wn(tag)
    if wn_tag is None:
        return None
    try:
        return wn.synsets(word, wn_tag)[0]
    except Exception as t:
        return None

def sentence_similarity(data1, data2):
    '''Compute the sentence similarity using Wordnet
    Input: two interactions (data1, data2)
    Output: similarity score'''
    # Get words from queries:
    words1 = word_tokenize(data1['query'])
    words2 = word_tokenize(data2['query'])
    # Tag the tokens: 
    sentence1 = pos_tag(words1)
    sentence2 = pos_tag(words2)
    # Get the synsets for the tagged words
    synsets1 = [tagged_to_synset(*tagged_word) for tagged_word in sentence1]
    synsets2 = [tagged_to_synset(*tagged_word) for tagged_word in sentence2]
    
    # Get words which are None in synsets and remove None from synset:
    unknown1 = [words1[i] for i in range(len(synsets1)) if not synsets1[i]]
    unknown2 = [words2[i] for i in range(len(synsets2)) if not synsets2[i]]
    synsets1 = [ss for ss in synsets1 if ss]
    synsets2 = [ss for ss in synsets2 if ss]
    
    # For unknown words; remove stop words and stem:
    stop_words = set(stopwords.words('english'))
    ps = PorterStemmer()
    unknown1 = [ps.stem(w) for w in unknown1 if not w in stop_words]
    unknown2 = [ps.stem(w) for w in unknown2 if not w in stop_words]

    score, scores = 0.0, 0.0

    # For each word in the first sentence
    for synset in synsets1:
        # Get the similarity value of the most similar word in the other sentence
        scores = [synset.path_similarity(ss) for ss in synsets2 if synset.path_similarity(ss) is not None ]
        #if len(scores) == 0:
        #        best_score = 0.0
        #else: best_score = max(scores)
        score += max(scores, default=0)
        # count += 1  Ik denk dat het hierdoor niet symmetrisch was
    count = max(len(synsets1), len(synsets2))
    # Compare the unknown words with eachother: 
    for w1 in unknown1:
        test = [(w1==w2)*1 for w2 in unknown2]
        score += max(test, default=0)
    count += max(len(unknown1), len(unknown2))
    
    # Average the values and return the score
    if count != 0:
        score = score/ count
    return score

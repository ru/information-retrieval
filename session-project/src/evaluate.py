'''
This file contains functions that can be used to evaluate the results.
'''
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from sklearn.metrics import confusion_matrix
from sklearn.metrics import roc_curve

def plotsim(simmsr, sameses, bins0=10000, bins1=100, density=False, title='Histogram of similarity measure'):
    '''Plots a histogram of the similarity measure between queries from different
    sessions and between queries of the same session. 
    Input:
    * simmsr : matrix with the similarity measure for all pairs of queries
    * sameses : matrix which shows if i and j are from the same session (1 if yes, 0 if no)
    * bins0=10000 : nr of bins for histogram of different sessions
    * bins1=100 : nr of bins for histogram of same sessions
    Output:
    * plot'''
    plt.figure()
    # Plot histogram of similarity measures for different sessions:
    plt.hist(simmsr[sameses==0.], bins=bins0, density=density, histtype='step', 
             color='b', label='Different sessions')
    # Plot histogram of similarity measures for same sessions:
    plt.hist(simmsr[sameses==1.], bins=bins1, density=density, histtype='step', 
             color='r', label='Same sessions')
    plt.xlim(0,1)
    plt.title(title)
    plt.xlabel('Similarity measure')
    plt.legend()
    plt.show()


def accsim(X, y, simmeas):
    '''Calculates the accuracy of the simmilarity measures for a range of thresholds
    Input:
    * X: predictions: the similarity measure for query pairs
    * y: targets: whether the query pairs are in the same session
    * simmeas: the list of similarity functions
    Output: dictionary of 'simmeas': list of [tp/(tp+fn), tn/(tn+fp)] for each threshold'''
    nrsm = len(simmeas)
    accuracy = np.zeros((3*nrsm,11))
    names = []
    for i in range(nrsm):
        names.append(simmeas[i].__name__ + ' Accuracy')
    for i in range(nrsm):
        names.append(simmeas[i].__name__ + ' Recall')
    for i in range(nrsm):
        names.append(simmeas[i].__name__ + ' Precision')
    for i in range(11):
        th = i/10.
        predict = np.zeros(np.shape(X))
        predict[X>=th] = 1.
        for j in range(nrsm):
            tn, fp, fn, tp = confusion_matrix(y.flatten(), predict[:,:,j].flatten()).ravel()
            #accuracy[simmeas[i].__name__].append([tp/(tp+fn), tn/(tn+fp)])
            accuracy[j,i] = (tp+tn)/(tn+fp+fn+tp)  # Accuracy
            accuracy[nrsm+j,i] = tp/(tp+fn)  # Recall
            accuracy[2*nrsm+j,i] = tp/(tp+fp)  # Precision
    return pd.DataFrame(data=accuracy, index=names, columns=np.arange(0,1.1,0.1))

def conf_matrix(X, y, simmeas):
    pass    

def ROC(X, y, simmeas):
    '''Plots the ROC curves for the different similarity measures.
    Input:
    * X: predictions: the similarity measure for query pairs
    * y: targets: whether the query pairs are in the same session
    * simmeas: the list of similarity functions
    Output: (plot)
    '''
    plt.figure()
    plt.title('ROC curve')
    for i in range(len(simmeas)):
        name = simmeas[i].__name__
        fpr, tpr, ths = roc_curve(y.flatten(), X[:,:,i].flatten())
        plt.plot(fpr, tpr, label=name)
    plt.xlabel('False positive rate')
    plt.ylabel('True positive rate')
    plt.legend()
    plt.show()
        
        



    